<?php

class task_model extends CI_Model
{

    function Create($text, $deadline){
        $text = $this->db->escape($text);
        $deadline = $this->db->escape($deadline);
        $query = "INSERT INTO Tasks (text,deadline) VALUES ($text, $deadline)";
        $this->db->query($query);
    }

    function ChangeState($id, $status){
        $query = "UPDATE Tasks SET status=$status WHERE id=$id";
        $this->db->query($query);
    }

    function FindTaskComments($array, $taskId){
        $comments = [];
        foreach ($array as $item){
            if($item['task_id'] == $taskId && $item['comment_id'] != NULL)
            {
                $comment['id'] = $item['comment_id'];
                $comment['name'] = $item['comment_name'];
                $comment['date'] = $item['comment_date'];
                $comment['text'] = $item['comment_text'];
                $comments[] = $comment;
            }
        }
        return $comments;
    }
    function CollectTaskObject($object){
        $data = [];
        foreach ($object as $task){
            foreach ($object as $task){
                $data[$task['task_id']] = array("id" => $task['task_id'],
                                                "text" => $task['task_text'],
                                                "status" => $task['task_status'],
                                                "deadline" => $task['task_deadline']);
            }
        }
        $ndata = [];
        foreach ($data as $taskId=>$task){
            $task['comments'] = $this->FindTaskComments($object, $task['id']);
            $ndata[] =$task;
        }

        return $ndata;
    }
    function Get($id){
        $query = "SELECT T.id as 'task_id',
                         T.text as 'task_text',
                         T.status as 'task_status',
                         T.deadline as 'task_deadline',
                         C.id as 'comment_id',
                         C.name as 'comment_name',
                         C.date as 'comment_date',
                         C.text as 'comment_text'
                         FROM Tasks as T LEFT JOIN Comments AS C ON T.id = C.task_id WHERE T.id=$id ORDER BY T.status, T.deadline, C.date";
        $result = $this->db->query($query);
        return $this->CollectTaskObject($result->result_array());
    }

    function GetList(){
        $query = "SELECT T.id as 'task_id',
                         T.text as 'task_text',
                         T.status as 'task_status',
                         T.deadline as 'task_deadline',
                         C.id as 'comment_id',
                         C.name as 'comment_name',
                         C.date as 'comment_date',
                         C.text as 'comment_text'
                         FROM Tasks as T LEFT JOIN Comments AS C ON T.id = C.task_id ORDER BY T.status, T.deadline, C.date";
        $result = $this->db->query($query);
        return $this->CollectTaskObject($result->result_array());
    }


}