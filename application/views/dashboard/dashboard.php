<!DOCTYPE html>
<html>
<head>
    <title>Dashboard</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/dashboard.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/libs/jquery-ui-1.11.4.custom/jquery-ui.min.css">
    <script src="<?=base_url()?>assets/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url()?>assets/libs/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
</head>
<body>
<div class="visibleTable">
    <table class="tasksList" border="2">
        <thead>
        <tr>
            <th>Status</th>
            <th>Text</th>
            <th>Deadline</th>
            <th>Comments</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($tasks as $key=>$value){
            $class = $value["status"]==1?"checked":"";
            echo '<tr class="'.$class.'" data-id="'.$value['id'].'">';
            echo '<td class="status"><input type="checkbox"';
            if($value["status"]==1)
                echo "checked";
            echo '/></td>';
            echo '<td><a href="'.base_url().'index.php/task/Detailed/'.$value['id'].'">'.$value['text'].'</a></td>';
            echo '<td>'.$value['deadline'].'</td>';
            echo '<td>'.count($value['comments']).'</td>';
            echo '</tr>';
        }
        ?>
        </tbody>
    </table>
</div>

<div class="insertNewTask">
    <input type="text" class="newTaskName" />
    <span class="deadline"><input type="text"/></span>
    <span class="addTask">Add</span>
</div>
</body>
<script>
    var base_url = '<?=base_url()?>';
</script>
<script src="<?=base_url()?>assets/js/task.js"></script>
<script src="<?=base_url()?>assets/js/dashboard.js"></script>
</html>
