<!DOCTYPE html>
<html>
<head>
    <title>Task</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/libs/jquery-ui-1.11.4.custom/jquery-ui.min.css">
    <script src="<?=base_url()?>assets/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url()?>assets/libs/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
</head>
<body>
<h2>Task:</h2>
<div class="task" data-id="<?=$task['id']?>">
    <span class="status">
       Status:
        <input type="checkbox" <?php if($task["status"]==1) echo "checked" ?> />
    </span>
    <div class="text">
        Text:
        <?=$task['text']?>
    </div>
    <span class="deadline">
        Deadline:
        <?=$task['deadline']?>
    </span>
</div>
<h2>Comments:</h2>
<table class="comments">
    <thead>
    <tr>
        <th>Name</th>
        <th>Text</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($task['comments'] as $comment) {
        echo '<tr>';
        echo '<td>' . $comment['name'] . '</td>';
        echo '<td>' . $comment['text'] . '</td>';
        echo '<td>' . $comment['date'] . '</td>';
        echo '</tr>';
    }
    ?>
    </tbody>
</table>
<div class="insertNewComment">
    <input type="text" class="newTaskName" name="name" placeholder="name" />
    <input type="text" name="comment" placeholder="comment"/>
    <span class="addComment">Add</span>
</div>
</body>
<script>
    var base_url = '<?=base_url()?>';
</script>
<script src="<?=base_url()?>assets/js/task.js"></script>
<script src="<?=base_url()?>assets/js/detailed.js"></script>
</html>
