<?php


class Task extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('task_model');
        $this->load->model('comment_model');
    }
    public function GetList(){
        echo json_encode($this->task_model->GetList());
    }
    public function Get($id){
        $res = $this->task_model->Get($id);
        if(count($res)>0)
            $data = $res[0];
        else $data = NULL;
        echo json_encode($data);
    }
    public function Detailed($id){
        $res = $this->task_model->Get($id);
        if(count($res)>0)
             $data['task'] = $res[0];
        else $data['task'] = NULL;
        $this->load->view('task/detailed', $data);
    }

    public function ChangeStatus(){
        if(isset($_POST['task_id']) && isset($_POST['status']) ){
            $this->task_model->ChangeState($_POST['task_id'],$_POST['status']);
        }
    }
    public function Add(){
        if(isset($_POST['task_name']) && isset($_POST['task_deadline'])){
            $this->task_model->Create($_POST['task_name'],$_POST['task_deadline']);
        }
    }
    public function Comment(){
        if(isset($_POST['task_id']) && isset($_POST['name']) &&isset($_POST['comment'])){
            $this->comment_model->Create($_POST['task_id'], $_POST['name'], $_POST['comment']);
        }
    }
}