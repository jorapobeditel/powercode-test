<?php

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('task_model');
    }

    public function index()
    {
        $data['tasks'] = $this->task_model->GetList();
        $this->load->view('dashboard/dashboard', $data);
    }
}