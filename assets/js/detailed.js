$(function () {
    function UpdateComments(task) {
        $('.comments tbody tr').remove();
        if(task!=null){
            for(var i = 0; i<task.comments.length; i++){
                var tr = $('<tr/>');
                $('<td/>', { text: task.comments[i].name}).appendTo(tr);
                $('<td/>', { text: task.comments[i].text}).appendTo(tr)
                $('<td/>', { text: task.comments[i].date}).appendTo(tr)
                $('.comments tbody').append(tr);
            }
        }

    }
    $('.addComment').click(function () {
        var name = $(this).siblings('input[name="name"]').val();
        var comment = $(this).siblings('input[name="comment"]').val();;
        if(name != '' && comment !=''){
            var taskId = $('.task').data('id');
            taskModel.Comment(taskId, name, comment);
            taskModel.Get(taskId, UpdateComments)

        }
        else alert("task name and task deadline can't be empty");
    });

    function bindStatusClick() {
        $('.status input').change(function () {
            var value = $(this).prop("checked")==true?1:0;
            var taskId = $('.task').data('id');
            taskModel.ChangeTaskStatus(taskId, value);
        });
    }
    bindStatusClick();
});