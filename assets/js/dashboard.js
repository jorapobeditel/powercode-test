$(function () {
    $('.deadline input').datepicker({
        dateFormat: "yy-mm-dd"
    });
    function updateTasks(tasks) {
        var getCheckBox = function (checked) {
            if(checked){
                return $('<input/>', {
                    "type": "checkbox",
                    "checked": "checked"
                });
            }else{
                return $('<input/>',{
                    "type": "checkbox"
                });
            }

        }

        $('.tasksList tbody tr').remove();
        for(var key in tasks){
            var tr = $('<tr/>', { "data-id": tasks[key].id});
            var status = $('<td/>', {
                class: "status",
            }).appendTo(tr);
            if(tasks[key].status==1) {
                status.append(getCheckBox(true));
                tr.addClass("checked");
            }
            else
                status.append(getCheckBox(false));
            var detailed = $('<a/>', {
                "href": base_url+'index.php/task/Detailed/'+tasks[key].id,
                text: tasks[key].text
            })
            $('<td/>').append(detailed).appendTo(tr);
            $('<td/>', { text: tasks[key].deadline}).appendTo(tr);
            $('<td/>', { text: tasks[key].comments.length}).appendTo(tr);
            $('.tasksList tbody').append(tr);
        }
        bindStatusClick();
    }
    function bindStatusClick() {
        $('.status input').change(function () {
            var value = $(this).prop("checked")==true?1:0;
            var task_id = $(this).parents('tr').data('id');
            taskModel.ChangeTaskStatus(task_id, value);
            taskModel.GetList(updateTasks);
        });
    }

    $('.addTask').click(function () {
        var task_name = $(this).siblings('input').val();
        var task_deadline = $(this).siblings('.deadline').find('input').val();
        if(task_name != '' && task_deadline !=''){
            taskModel.Add(task_name, task_deadline);
            taskModel.GetList(updateTasks);
        }
        else alert("task name and task deadline can't be empty");
    })

    bindStatusClick();
});
