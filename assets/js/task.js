var taskModel = {
    Get: function (id, callback) {
        setTimeout(function () {
            $.ajax({
                method: "POST",
                url: base_url + 'index.php/Task/Get/' + id,
            })
                .done(function (result) {
                    callback(JSON.parse(result));
                })
                .fail(function (result) {
                    return false;
                })
        }, 500);
    },
    GetList: function (callback) {
        setTimeout(function () {
            $.ajax({
                method: "POST",
                url: base_url+'index.php/Task/GetList',
            })
                .done(function (result) {
                    callback(JSON.parse(result));
                })
                .fail(function (result) {
                    return false;
                })
        }, 500);

    },
    ChangeTaskStatus: function (task_id, status) {
        $.ajax({
            method: "POST",
            url: base_url+'index.php/Task/ChangeStatus',
            data: {
                task_id: task_id,
                status: status
            }
        })
            .done(function (result) {
                return true;
            })
            .fail(function (result) {
                return false;
            })
    },
    Add: function (task_name, task_deadline) {
        $.ajax({
            method: "POST",
            url: base_url+'index.php/Task/Add',
            data: {
                task_name: task_name,
                task_deadline: task_deadline
            }
        })
            .done(function (result) {
                return true;
            })
            .fail(function (result) {
                return false;
            })
    },
    Comment: function (task_id, name, comment) {
        $.ajax({
            method: "POST",
            url: base_url+'index.php/Task/Comment',
            data: {
                task_id: task_id,
                name: name,
                comment: comment
            }
        })
            .done(function (result) {
                return true;
            })
            .fail(function (result) {
                return false;
            })
    }
}